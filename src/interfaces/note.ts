export interface Note {
  title: string,
  author: string,
  text: string
}
