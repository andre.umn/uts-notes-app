import { Note } from '../interfaces/note';

export class NoteService {
  notes: Note[] = [];

  getNotes(){
    return this.notes;
  }
  addNote(note: Note){
    // console.log(note);
    this.notes.push(note);
  }
  removeNote(title){
    let idx = this.notes.findIndex((element) => {
      return element.title == title;
    });
    this.notes.splice(idx, 1);
  }
}
