import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { NoteService } from '../../services/noteService';
import { Note } from '../../interfaces/note';

@IonicPage()
@Component({
  selector: 'page-add-note',
  templateUrl: 'add-note.html',
})
export class AddNotePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private noteSvc: NoteService) {}

  closeModal(){
    this.viewCtrl.dismiss();
  }

  addNote(f: NgForm){

    let n: Note;
    n = {
      title: f.value.title,
      author: f.value.author,
      text: f.value.text
    }

    this.noteSvc.addNote(n);
    this.viewCtrl.dismiss();
  }
}
