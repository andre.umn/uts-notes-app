import { Component } from '@angular/core';
import { NavController, AlertController, ModalController, ToastController } from 'ionic-angular';
import { NoteService } from '../../services/noteService';
import { Note } from '../../interfaces/note';
import { AddNotePage } from '../add-note/add-note';
import { DetailsPage } from '../details/details';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  username: string;
  notes: Note[];
  isNullNotes = true;
  constructor(public navCtrl: NavController, private alertCtrl: AlertController,
    private noteSvc: NoteService, private modalCtrl: ModalController, private toastCtrl: ToastController) {}

  ionViewDidLoad(){
    let alert = this.alertCtrl.create({
      title: "Please write your name",
      inputs: [
        {
          name: 'username',
          placeholder: 'your name here'
        }
      ],
      buttons: [
        {
          text: "OK",
          handler: (data) => {
            this.username = data.username;
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();

  }
  ionViewWillEnter(){
    this.notes=this.noteSvc.getNotes();
    if(this.notes == []) this.isNullNotes = true;
    else this.isNullNotes = false;
  }

  removeNote(title){
    this.noteSvc.removeNote(title);
    let toast = this.toastCtrl.create({
      message: "Note has been deleted.",
      position: "bottom",
      duration: 3000
    });
    toast.present();
  }

  showAddModal(){
    let modal = this.modalCtrl.create(AddNotePage);
    modal.present();
  }

  showDetail(n: Note){
    this.navCtrl.push(DetailsPage, {note: n.text});
  }

  checkNotes(){
    this.notes=this.noteSvc.getNotes();
    if(this.notes == []) this.isNullNotes = true;
    else this.isNullNotes = false;
  }
}
